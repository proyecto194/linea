?php


?>

<!DOCTYPE html>
<html lang="en">
  
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content=
        "width=device-width, initial-scale=1.0">
    <title>
        Formulario 
    </title>
</head>
  
<body>
    <h1 style="color:#fff ;">Formulario</h1>
  
    <!-- Create Form -->
    <form id="form">
 
        <!-- Details -->
        <div class="form-control">
            <label for="name" id="label-name">
                Nombre Completo
            </label>
 
            <!-- Input Type Text -->
            <input type="text"
                   id="name"
                   placeholder="Ingrese su nombre" />
        </div>
  
        <div class="form-control">
            <label for="Correo electronico" id="label-email">
                Correo
            </label>
 
            <!-- Input Type Email-->
            <input type="email"
                   id="email"
                   placeholder="Ingrese su correo" />
        </div>
        <div class="form-control">
            <label>
            ¿Han sido identificados todos los procesos del sistema?
            </label>
            <label for="r1">
                <input type="radio"
                       id="r1-1"
                       name="1">Cumple</input>
                       <input type="radio"
                       id="r1-2"
                       name="2">No Cumple</input>
                       <input type="radio"
                       id="r1-3"
                       name="3">Cumple al 50%</input>
                       <input type="radio"
                       id="r1-4"
                       name="4">Cumple al 75%</input>
            </label>       
        </div>
        <div class="form-control">
            <label>¿Los procesos sub-contratados a proveedores externos, han sido identificados y controlados?</label>
            <label for="r2">
                <input type="radio"
                       id="r2-1"
                       name="5">Cumple</input>
                       <input type="radio"
                       id="r2-2"
                       name="6">No Cumple</input>
                       <input type="radio"
                       id="r2-3"
                       name="7">Cumple al 50%</input>
                       <input type="radio"
                       id="r2-4"
                       name="8">Cumple al 75%</input>
            </label>
        </div>
        <div class="form-control">           
            <label>  ¿La organización cuenta con un documento de política de la calidad?</label>
            <label for="r3">
                <input type="radio"
                       id="r3-1"
                       name="9">Cumple</input>
                       <input type="radio"
                       id="r3-2"
                       name="10">No Cumple</input>
                       <input type="radio"
                       id="r3-3"
                       name="11">Cumple al 50%</input>
                       <input type="radio"
                       id="r3-4"
                       name="12">Cumple al 75%</input>
            </label>
        </div>
        <div class="form-control">           
            <label>¿Existe otro documento en donde se discriminen los objetivos de calidad?</label>
            <label for="r4">
                <input type="radio"
                       id="r4-1"
                       name="13">Cumple</input>
                       <input type="radio"
                       id="r4-2"
                       name="14">No Cumple</input>
                       <input type="radio"
                       id="r4-3"
                       name="15">Cumple al 50%</input>
                       <input type="radio"
                       id="r4-4"
                       name="16">Cumple al 75%</input>
            </label>
        </div>
        <div class="form-control">           
            <label>¿Se cuenta con el manual de calidad?</label>
            <label for="r5">
                <input type="radio"
                       id="r5-1"
                       name="17">Cumple</input>
                       <input type="radio"
                       id="r5-2"
                       name="18">No Cumple</input>
                       <input type="radio"
                       id="r5-3"
                       name="19">Cumple al 50%</input>
                       <input type="radio"
                       id="r5-4"
                       name="20">Cumple al 75%</input>
            </label>
        </div>
        <div class="form-control">           
            <label>¿Los procedimientos exigidos por la norma, han sido documentados?</label>
            <label for="r6">
                <input type="radio"
                       id="r6-1"
                       name="21">Cumple</input>
                       <input type="radio"
                       id="r6-2"
                       name="22">No Cumple</input>
                       <input type="radio"
                       id="r6-3"
                       name="23">Cumple al 50%</input>
                       <input type="radio"
                       id="r6-4"
                       name="24">Cumple al 75%</input>
            </label>
        </div>
        <div class="form-control">           
            <label>¿En el manual, se da alcance al Sistema de Gestión de la Calidad?</label>
            <label for="r7">
                <input type="radio"
                       id="r7-1"
                       name="25">Cumple</input>
                       <input type="radio"
                       id="r7-2"
                       name="26">No Cumple</input>
                       <input type="radio"
                       id="r7-3"
                       name="27">Cumple al 50%</input>
                       <input type="radio"
                       id="r7-4"
                       name="28">Cumple al 75%</input>
            </label>
        </div>
        <div class="form-control">           
            <label>¿En el manual de la calidad, se incluyen las exclusiones contenidas en el apartado 7 y la justificación de su existencia?</label>
            <label for="r8">
                <input type="radio"
                       id="r8-1"
                       name="29">Cumple</input>
                       <input type="radio"
                       id="r8-2"
                       name="30">No Cumple</input>
                       <input type="radio"
                       id="r8-3"
                       name="31">Cumple al 50%</input>
                       <input type="radio"
                       id="r8-4"
                       name="32">Cumple al 75%</input>
            </label>
        </div>
        <div class="form-control">           
            <label>¿Los procedimientos documentados, forman parte del manual de calidad?</label>
            <label for="r9">
                <input type="radio"
                       id="r9-1"
                       name="33">Cumple</input>
                       <input type="radio"
                       id="r9-2"
                       name="34">No Cumple</input>
                       <input type="radio"
                       id="r9-3"
                       name="35">Cumple al 50%</input>
                       <input type="radio"
                       id="r9-4"
                       name="36">Cumple al 75%</input>
            </label>
        </div>
        <div class="form-control">           
            <label>¿El control de documentos cuenta con un procedimiento documentado?</label>
            <label for="r10">
                <input type="radio"
                       id="r10-1"
                       name="37">Cumple</input>
                       <input type="radio"
                       id="r10-2"
                       name="38">No Cumple</input>
                       <input type="radio"
                       id="r10-3"
                       name="39">Cumple al 50%</input>
                       <input type="radio"
                       id="r10-4"
                       name="40">Cumple al 75%</input>
            </label>
        </div>
        <div class="form-control">           
            <label>¿La metodología documentada, es adecuada para la aprobación de documentos?</label>
            <label for="r11">
                <input type="radio"
                       id="r11-1"
                       name="41">Cumple</input>
                       <input type="radio"
                       id="r11-2"
                       name="42">No Cumple</input>
                       <input type="radio"
                       id="11-3"
                       name="43">Cumple al 50%</input>
                       <input type="radio"
                       id="r11-4"
                       name="44">Cumple al 75%</input>
            </label>
        </div>
        <div class="form-control">           
            <label>¿La metodología documentada es adecuada para identificar los cambios de los documentos, de acuerdo con el estado de la versión vigente?</label>
            <label for="r12">
                <input type="radio"
                       id="r12-1"
                       name="45">Cumple</input>
                       <input type="radio"
                       id="r12 -2"
                       name="46">No Cumple</input>
                       <input type="radio"
                       id="r12-3"
                       name="47">Cumple al 50%</input>
                       <input type="radio"
                       id="r12-4"
                       name="48">Cumple al 75%</input>
            </label>
        </div>
        <div class="form-control">           
            <label>¿Los documentos revisados, están de acuerdo con la metodología de actualización y revisión?</label>
            <label for="r13">
                <input type="radio"
                       id="r13-1"
                       name="49">Cumple</input>
                       <input type="radio"
                       id="r6-2"
                       name="50">No Cumple</input>
                       <input type="radio"
                       id="r6-3"
                       name="51">Cumple al 50%</input>
                       <input type="radio"
                       id="r6-4"
                       name="52">Cumple al 75%</input>
            </label>
        </div>
        <div class="form-control">           
            <label>¿La metodología documentada es adecuada para identificar los cambios de los documentos, de acuerdo con el estado de la versión vigente?</label>
            <label for="r14">
                <input type="radio"
                       id="r14-1"
                       name="53">Cumple</input>
                       <input type="radio"
                       id="r14-2"
                       name="54">No Cumple</input>
                       <input type="radio"
                       id="14-3"
                       name="55">Cumple al 50%</input>
                       <input type="radio"
                       id="14-4"
                       name="56">Cumple al 75%</input>
            </label>
        </div>
        <div class="form-control">           
            <label>¿La distribución de documentos, cuenta con metodología documentada que permita que estos se encuentren disponibles en los puestos de trabajo?</label>
            <label for="r15">
                <input type="radio"
                       id="r15-1"
                       name="57">Cumple</input>
                       <input type="radio"
                       id="r15-2"
                       name="58">No Cumple</input>
                       <input type="radio"
                       id="r15-3"
                       name="59">Cumple al 50%</input>
                       <input type="radio"
                       id="r15-4"
                       name="60">Cumple al 75%</input>
            </label>
        </div>
        <div class="form-control">           
            <label>¿Los documentos revisados, están de acuerdo con la metodología de distribución de documentos?</label>
            <label for="r16">
                <input type="radio"
                       id="r16-1"
                       name="61">Cumple</input>
                       <input type="radio"
                       id="r16-2"
                       name="62">No Cumple</input>
                       <input type="radio"
                       id="r16-3"
                       name="63">Cumple al 50%</input>
                       <input type="radio"
                       id="r16-4"
                       name="64">Cumple al 75%</input>
            </label>
        </div>
        <div class="form-control">           
            <label>¿Los documentos de origen externo, son controlados en forma adecuada y se garantiza su distribución?</label>
            <label for="r17">
                <input type="radio"
                       id="r17-1"
                       name="65">Cumple</input>
                       <input type="radio"
                       id="r17-2"
                       name="66">No Cumple</input>
                       <input type="radio"
                       id="r17-3"
                       name="67">Cumple al 50%</input>
                       <input type="radio"
                       id="r17-4"
                       name="68">Cumple al 75%</input>
            </label>
        </div>
        <div class="form-control">           
            <label>¿El uso de documentos obsoletos, está regulado por una metodología que evite su distribución?</label>
            <label for="r18">
                <input type="radio"
                       id="r18-1"
                       name="69">Cumple</input>
                       <input type="radio"
                       id="r18-2"
                       name="70">No Cumple</input>
                       <input type="radio"
                       id="r18-3"
                       name="71">Cumple al 50%</input>
                       <input type="radio"
                       id="r18-4"
                       name="72">Cumple al 75%</input>
            </label>
        </div>
        <div class="form-control">           
            <label>¿Estos documentos obsoletos, son tratados de acuerdo con la metodología definida para ello?</label>
            <label for="r19">
                <input type="radio"
                       id="r19-1"
                       name="73">Cumple</input>
                       <input type="radio"
                       id="r19-2"
                       name="74">No Cumple</input>
                       <input type="radio"
                       id="r19-3"
                       name="75">Cumple al 50%</input>
                       <input type="radio"
                       id="r19-4"
                       name="76">Cumple al 75%</input>
            </label>

       <br>
        <div class="form-control">
            <label for="comment">
                Any comments or suggestions
            </label>
 
            <!-- multi-line text input control -->
            <textarea name="comment" id="comment"
                placeholder="Enter your comment here">
            </textarea>
        </div>
  
        <!-- Multi-line Text Input Control -->
        <button type="submit" value="submit">
            Submit
        </button>
    </form>
   

</body>
  
<style>
    body {
        background-color: #0432ff;
        font-family: Verdana;
        text-align: center;
    }
    
    form {
        background-color: #fff;
        max-width: 700px;
        margin: 40px auto;
        padding: 20px 30px;
        box-shadow: 2px 5px 10px rgba(0, 0, 0, 0.5);
    }

    
    .form-control {
        text-align: justify;
        margin-bottom: 25px;
    }

    
    .form-control label {
        display: block;
        margin-bottom: 10px;
    }

    .form-control input,
    .form-control select,
    .form-control textarea {
        border: 1px solid #777;
        border-radius: 2px;
        font-family: inherit;
        padding: 10px;
        display: block;
        width: 95%;
    }

   
    .form-control input[type="radio"],
    .form-control input[type="checkbox"] {
        display: inline-block;
        width: auto;
    }

    button {
        background-color: #05c46b;
        border: 1px solid #777;
        border-radius: 2px;
        font-family: inherit;
        font-size: 21px;
        display: block;
        width: 100%;
        margin-top: 50px;
        margin-bottom: 20px;
    }
    
</style>
</html>